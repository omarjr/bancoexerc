
public class TestaContas {
	
	public static void main(String[] args) {
		Conta umaConta = new Conta();
		Conta umaContaCorrente = new ContaCorrente();
		Conta umaContaPoupanca = new ContaPoupanca();

		umaConta.deposita(1000);
		umaContaCorrente.deposita(1000);
		umaContaPoupanca.deposita(1000);

		System.out.println("Saldos das contas após o depósito:");
		System.out.println("Saldo da Conta: " + umaConta.getSaldo());
		System.out.println("Saldo da Conta Corrente: " + umaContaCorrente.getSaldo());
		System.out.println("Saldo da Conta Poupança: " + umaContaPoupanca.getSaldo());
		System.out.println("");
		
		umaConta.atualiza(0.01);
		umaContaCorrente.atualiza(0.01);
		umaContaPoupanca.atualiza(0.01);
		
		System.out.println("Saldos das contas após a atualização com a taxa:");
		System.out.println("Saldo da Conta: " + umaConta.getSaldo());
		System.out.println("Saldo da Conta Corrente: " + umaContaCorrente.getSaldo());
		System.out.println("Saldo da Conta Poupança: " + umaContaPoupanca.getSaldo());
		/*Após imprimir o saldo das três contas, percebe-se que o valor foi atualizado
		somando-se o valor da taxa ao valor do saldo que por sua vez havia recebido o 
		depósito de 1000.*/
		
	}

}
